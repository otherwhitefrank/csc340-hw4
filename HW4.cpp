/*
		Name: Frank Dye
		Student ID: 912927332
		Date: 10/7/2013
		Description: 
			Q1: A simple search function to find a product given in two arrays. Throws an exception if not found
			Q2: A simple Account object that has basic balance, withdraw, deposit functionality, throws exceptions
			    if Overdrawn, negative deposit, etc.
*/


#include <iostream>
#include <string>
#include "Account.h"
#include "Exceptions.h"

using namespace std;

//Prototypes
int getProductID(int ids[], string names[], int numProducts, string target) throw (Exception_ID_Not_Found);

int getProductID(int ids[], string names[], int numProducts, string target)
{
	bool found = false;
	int id = -9999;

	for (int i=0; i < numProducts; i++)
	{
		if (names[i] == target)
		{
			id = ids[i];
			found = true;
		}
	}

	if (!found)
	{
		throw Exception_ID_Not_Found();

	}

	return id; // not found
}

int main() // Sample code to test the getProductID function
{
	int productIds[]= {4, 5, 8, 10, 13};
	string products[] = { "computer", "flash drive", "mouse", "printer", "camera" };

	string temp;

	
	try {
		//Q1
		cout << getProductID(productIds, products, 5, "mouse") << endl;
		cout << getProductID(productIds, products, 5, "camera") << endl;
		cout << getProductID(productIds, products, 5, "laptop") << endl;

		//Q2
		Account testAccount1(323.00);
		Account testAccount2(-450);
		
		cout << "testAccount1 balance: " << testAccount1.withdraw(450) << endl;
		cout << "testAccount2 balance: " << testAccount2.getBalance() << endl;
		cout << "testAccount1 balance: " << testAccount1.deposit(-350) << endl;
	} 
	catch (const Exception_ID_Not_Found& e)
	{
		//ID Not found
		cerr << e.what() << endl;
		

	}
	catch (const Exception_Negative_Deposit& e)
	{

		//Exception caught
		cerr << e.what() << endl;
		

	}
	catch (const Exception_Negative_Withdraw& e)
	{
		cerr << e.what() << endl;
		
	}
	catch (const Exception_Overdrawn& e)
	{
		cerr << e.what() << endl;
		
	}
	catch (...)
	{
		cerr << "Unhandled Exception!" << endl;
	}

	cout << "Press any key to continue";
	cin >> temp;

	return 0;
}
