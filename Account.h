/*

		Name: Frank Dye
		Student ID: 912927332
		Date: 10/7/2013
		Description: Simple account object with basic balance, withdraw, deposit functions

*/


#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <iostream>
#include "Exceptions.h"

class Account
{
private:
	double balance;
public:
	Account(); //Standard constructor
	Account(double initialDeposit); //Constructor with initial value
	double getBalance() throw (Exception_Overdrawn); //Get account balance
	double deposit(double amount) throw (Exception_Negative_Deposit); //Deposit into account
	double withdraw(double amount) throw (Exception_Negative_Withdraw); //Withdraw from account
};

#endif