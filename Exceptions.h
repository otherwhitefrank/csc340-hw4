/*

		Name: Frank Dye
		Student ID: 912927332
		Date: 10/7/2013
		Description: Defines several exceptions needed in the Account object

*/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

using namespace std;

class Exception_ID_Not_Found : public std::exception
{
public:

	virtual const char* what() const throw()
	{
		return "Exception: ID Not Found!";
	};

};


class Exception_Negative_Withdraw : private exception
{
public:
	virtual const char* what() const throw()
	{
		return "Exception: Negative Withdraw!";
	};

};


class Exception_Overdrawn : public exception
{
public:

	virtual const char* what() const throw()
	{
		return "Exception: Account Overdrawn!";
	};

};


class Exception_Negative_Deposit : public exception
{
public:
	virtual const char* what() const throw()
	{
		return "Exception: Negative Deposit!";
	};

};

#endif