/*

		Name: Frank Dye
		Student ID: 912927332
		Date: 10/7/2013
		Description: Simple account object with basic balance, withdraw, deposit functions

*/

#include "Account.h"

Account :: Account()
{
	balance = 0;
}

Account :: Account(double initialDeposit)
{
	balance = initialDeposit;
}

//returns balance or throws Exception_Overdrawn if the account is overdrawn.
double Account::getBalance()
{
	if (balance < 0)
		throw Exception_Overdrawn();
	else
		return balance;

	return balance;
}

// returns new balance or throws exception if error
double Account::deposit(double amount)
{
	if (amount > 0)
		balance += amount;
	else
		throw Exception_Negative_Deposit(); // code indicating error
	return balance;
}

// returns new balance or throws exception if invalid amount
double Account::withdraw(double amount)
{
	if((amount > balance) || (amount < 0))
		throw Exception_Negative_Withdraw();
	else
		balance -= amount;
	return balance;
}